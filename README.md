## Very simple banking servlet webapplication

### Getting started

Included Maven profiles for initialize database and schema.

Set up the Maven properties to your database accordigly.

These are for initialize:

```
		<db.host>[HOST]</db.host>  
		<db.port>[PORT]</db.port>  
		<db.user>[POSTGRES USER]</db.user>  
		<db.pass>[POSTGRES PASSWORD]</db.pass>  
```

and run with:

```
mvn -P init-db -am clean initialize 
```

These for Flyway migrate:

```
		<db.app.dbname>bank_db</db.app.dbname>  
		<db.app.dbname>bank_db</db.app.dbname>  
		<db.app.dbschema>bank_schema</db.app.dbschema>  
		<db.app.username>banking_app</db.app.username>  
		<db.app.password>Waaaah123</db.app.password>  
```

run with:

```
mvn flyway:migrate
```

_The application automatically migrates on startup._

There are three user in the insert scripts:

Name: Kane  
Email: kane@localhost  
Pass: Jelszo123  
Accounts: 2 EUR, 1 HUF

Name: Waluigi  
Email: waluigi@localhost  
Pass: Jelszo456  
Accounts: 1 EUR, 2 HUF

Name: Samus  
Email: samus@localhost  
Pass: Jelszo123  
Accounts: 0 EUR, 0 HUF

### Runtime
Tested on Wildfly 15, depends on Wildfly logging.

There are no external configuration yet for setting the datasource. 
The Datasource defaultly uses the properties file under /src/main/resources/datasource/local.properties. Set up accordingly before building the application.

Alternative way to run is with webapp-runner.

Build:
```
mvn -Dmaven.test.skip=true -P webapp-runner -am clean install
```

and run:
```
java -jar target/dependency/webapp-runner.jar target/*.war
```

### Database
Tested with Postgresql 11.