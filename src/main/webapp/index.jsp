<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="resources/css/site.css">

<title>Web banking application</title>
</head>
<body>
	<jsp:include page="WEB-INF/pages/components/menubar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-4">Yet another banking application.</h1>
		<p class="lead">Welcome to the next level ultimate Banking web
			application!</p>
	</div>

	<c:if test="${sessionScope.user == null}">
		<div class="container-fluid" style="">
			<form action="login" method="POST">
				<div class="form-group">
					<label for="login_email">Email address</label> <input type="email"
						class="form-control" id="login_email" name="email"
						aria-describedby="emailHelp" placeholder="Enter email">
				</div>
				<div class="form-group">
					<label for="login_pass">Password</label> <input type="password"
						class="form-control" id="login_pass" name="pass"
						placeholder="Password">
				</div>
				<div class="form-group form-check">
					<input type="checkbox" class="form-check-input" id="remember_me"
						name="remember"> <label class="form-check-label"
						for="exampleCheck1">Remember me</label>
				</div>
				<button type="submit" class="btn btn-primary">Sign in</button>
	
				<c:if test="${errorMessage != null}">
					<div class="alert alert-danger" role="alert" style="margin-top: 1em">
						<span><c:out value="${errorMessage}"></c:out> </span>
					</div>
				</c:if>
			</form>
	
			<div
				class="alert alert-warning alert-dismissible fade show alert-fixed"
				role="alert">
				<strong>Our site uses cookies.</strong> Now you know.
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
	</c:if>

	<script src="resources/jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="resources/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
