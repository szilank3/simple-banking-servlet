<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<div class="container-fluid">
	<p class="h3">Account history</p>
	<c:choose>
		<c:when test="${not empty transactions}">
			<table class="table table-striped">
				<thead>
					<tr>
						<th scope="col">Transaction type</th>
						<th scope="col">Account number</th>
						<th scope="col">Amount</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${transactions}" var="transaction">
						<tr>
							<th scope="row">${transaction.transactionType.displayName}</th>
							<td>${transaction.bankAccount.iban}</td>
							<td><fmt:formatNumber type = "number" groupingUsed="true"
         						maxFractionDigits = "0" value = "${transaction.amount}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<p class="lead">No transactions yet.</p>
		</c:otherwise>
	</c:choose>
</div>