<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<div class="container-fluid">
	<p class="h3">Accounts</p>
	<c:choose>
		<c:when test="${not empty accounts}">
			<table class="table table-striped">
				<thead>
					<tr>
						<th scope="col">Account number</th>
						<th scope="col">Currency</th>
						<th scope="col">Balance</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${accounts}" var="account">
						<tr>
							<th scope="row"><a href="${pageContext.request.contextPath}/secured/transactions?account_id=${account.accountId}" class="badge badge-primary">${account.iban}</a></th>
							<td>${account.currency}</td>
							<td><fmt:formatNumber type = "number" groupingUsed="true"
         						maxFractionDigits = "0" value = "${account.balance}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<p class="lead">No available accounts.</p>
		</c:otherwise>
	</c:choose>
</div>