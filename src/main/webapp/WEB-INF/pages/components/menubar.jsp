<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="user" scope="session" value="${sessionScope.user}" />

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<a class="navbar-brand" href="#">Yaba</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbar_toggler" aria-controls="navbar_toggler"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbar_toggler">

		<c:choose>
			<c:when test="${user != null}">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/secured/dashboard">Dashboard
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/secured/transfer">Transfer</a></li>
				</ul>

				<span class="navbar-text">Hello, <c:out value="${user.username}"></c:out> </span>

				<button type="button" class="btn btn-primary" data-toggle="modal"
					data-target="#logoutModal">Logout</button>
			</c:when>
			<c:otherwise>
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}">Home
							<span class="sr-only">(current)</span>
					</a></li>
				</ul>
			</c:otherwise>
		</c:choose>

	</div>
</nav>

<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
	aria-labelledby="logoutModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="logoutModalLabel">Logout</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">Are you sure to want to logout?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<form action="${pageContext.request.contextPath}/logout" method="post">
					<button type="submit" class="btn btn-primary">Logout</button>
				</form>
			</div>
		</div>
	</div>
</div>