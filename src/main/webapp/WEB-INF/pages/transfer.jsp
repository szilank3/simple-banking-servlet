<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css">
<!-- Custom CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/site.css">

<title>Transactions</title>
</head>
<body>
	<jsp:include page="components/menubar.jsp"></jsp:include>
	<c:set var="transfer" value="${sessionScope.transfer_state}"></c:set>

	<div class="container-fluid">
		<p class="h3">Transfer</p>
		<c:choose>
			<c:when test="${transfer != null}">
				<c:set var="sourceAcc" value="${sessionScope.transfer_state.sourceBankAccount}"></c:set>
				<c:set var="destAcc" value="${sessionScope.transfer_state.destinationBankAccount}"></c:set>
						<!-- Readonly form -->
						<form>
							<fieldset disabled>
					            <div class="form-group">
				                  <label for="sourceAccount">Source account</label> 
				                  <input type="text" class="form-control" id="sourceAccount" value="${sourceAcc.iban} (${sourceAcc.currency}) Balance: ${sourceAcc.balance}">
				                </div>
					            <div class="form-group">
				                  <label for="destinationAccount">Source account</label> 
				                  <input type="text" class="form-control" id="destinationAccount" value="${destAcc.iban} (${destAcc.currency}) Balance: ${destAcc.balance}">
				                </div>
								<div class="form-group">
									<label for="amount">Amount</label>
									<input id="amount" type="number" class="form-control" name="amount" value="${transfer.amount}"
										aria-label="Amount (Positive number without decimal)">
								</div>
							</fieldset>
						</form>			
						
						<div class="form-inline">
							<form action="${pageContext.request.contextPath}/secured/transfer"
								method="post">
								<button type="submit" class="btn btn-primary mr-2">Back</button>
							</form>
							
							<form id="submit_form" action="${pageContext.request.contextPath}/secured/transfer"
								method="post">
								<input type="hidden" name="option" value="submit">
								<button type="submit" form="submit_form" class="btn btn-primary">Finalize</button>
							</form>
						</div>
			</c:when>
			<c:otherwise>
						<form id="transfer_form"
							action="${pageContext.request.contextPath}/secured/transfer"
							method="post">
							<fieldset> 
								<input type="hidden" name="option" value="check">
								<div class="form-group">
									<label for="sourceAccount">Source account</label> <select
										class="form-control" id="sourceAccount" name="source_account">
										<c:forEach items="${accounts}" var="account">
											<option value="${account.accountId}">${account.iban} (${account.currency}) Balance: <fmt:formatNumber type = "number" groupingUsed="true"
         										maxFractionDigits = "0" value = "${account.balance}" /></option>
										</c:forEach>
									</select>
								</div>
								<div class="form-group">
									<label for="destinationAccount">Destination account</label> <select
										class="form-control" id="destinationAccount"
										name="destination_account">
										<c:forEach items="${accounts}" var="account">
											<option value="${account.accountId}">${account.iban} (${account.currency}) Balance: ${account.balance}</option>
										</c:forEach>
									</select>
									<small id="transferHelp" class="text-muted">
					                	The source and destination account must be not the same and must be in same currency
					                </small>
								</div>
								<div class="form-group">
									<label for="amount">Amount</label>
									<input id="amount" type="number" class="form-control" name="amount"
										aria-label="Amount (Positive number without decimal)">
								</div>
							</fieldset>
						</form>
						
						<div class="form-inline">
							<form action="${pageContext.request.contextPath}/secured/transfer?option=cancel"
								method="post">
								<button type="submit" class="btn btn-primary mr-2">Cancel</button>
							</form>
				
							<button type="submit" form="transfer_form" class="btn btn-primary">Check</button>
						</div>
			</c:otherwise>
		</c:choose>
	</div>
	
	<c:if test="${errorMessage != null}">
		<div class="alert alert-danger" role="alert" style="margin-top: 1em">
			<span><c:out value="${errorMessage}"></c:out> </span>
		</div>
	</c:if>

	<script
		src="${pageContext.request.contextPath}/resources/jquery/jquery-3.3.1.slim.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>