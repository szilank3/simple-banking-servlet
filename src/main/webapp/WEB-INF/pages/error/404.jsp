<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Page not found :(</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/pure-min.css" />
<style type="text/css">
.img {
	position: relative;
}

h1 { 
   position: absolute; 
   top: 5%; 
   left: 2%; 
   width: 100%;
   text-shadow: 2px 2px rgba(250, 250, 250, 0.7);
}

.waluigi {
	max-width: 33%; 
    max-height: 33%;
}
</style>
</head>
<body>
	<div class="pure-img img">
		<img class="waluigi" alt="waluigi" src="${pageContext.request.contextPath}/resources/assets/waluigi.jpg">
		<h1>This is not the WAAAAAH you looking for!</h1>
	</div>
</body>
</html>