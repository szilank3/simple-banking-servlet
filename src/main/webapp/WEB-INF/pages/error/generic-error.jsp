<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/site.css">

<title>Banking went wrong</title>
</head>
<body>
	<jsp:include page="../components/menubar.jsp"></jsp:include>

	<div class="jumbotron">
		<h1 class="display-4">Bugger!</h1>
		<p class="lead">Something went wrong under da hood. If the error persists contact Jesus.</p>
	</div>

	<script src="${pageContext.request.contextPath}/resources/jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
