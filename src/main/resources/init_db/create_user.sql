CREATE USER ${db.app.username} WITH ENCRYPTED PASSWORD '${db.app.password}';
/
GRANT ALL PRIVILEGES ON DATABASE ${db.app.dbname} TO ${db.app.username};
/