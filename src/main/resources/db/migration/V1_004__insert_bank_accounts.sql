INSERT INTO bank_accounts(user_id, currency, iban, balance, create_date, last_update_date, active)
	VALUES ((SELECT user_id FROM users WHERE username = 'Kane'), 'HUF', 'HU88403672250391247542141943', 50000, now(), now(), true);
INSERT INTO bank_accounts(user_id, currency, iban, balance, create_date, last_update_date, active)
	VALUES ((SELECT user_id FROM users WHERE username = 'Waluigi'), 'EUR', 'HU87860522307795071607523875', 500, now(), now(), true);
INSERT INTO bank_accounts(user_id, currency, iban, balance, create_date, last_update_date, active)
	VALUES ((SELECT user_id FROM users WHERE username = 'Kane'), 'EUR', 'HU88403672250391247542141986', 500, now(), now(), true);
INSERT INTO bank_accounts(user_id, currency, iban, balance, create_date, last_update_date, active)
	VALUES ((SELECT user_id FROM users WHERE username = 'Waluigi'), 'HUF', 'HU87860522307795071607523880', 50000, now(), now(), true);
INSERT INTO bank_accounts(user_id, currency, iban, balance, create_date, last_update_date, active)
	VALUES ((SELECT user_id FROM users WHERE username = 'Kane'), 'EUR', 'HU14630352096153436639490645', 500, now(), now(), true);
INSERT INTO bank_accounts(user_id, currency, iban, balance, create_date, last_update_date, active)
	VALUES ((SELECT user_id FROM users WHERE username = 'Waluigi'), 'HUF', 'HU23470717731410945650430701', 50000, now(), now(), true);
