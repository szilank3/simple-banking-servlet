CREATE TABLE users 
(
	user_id BIGSERIAL PRIMARY KEY,
	username VARCHAR(30) NOT NULL,
	salt VARCHAR(32) NOT NULL,
	password VARCHAR(512) NOT NULL,
	email VARCHAR(200) NOT NULL,
	create_date TIMESTAMP WITH TIME ZONE NOT NULL,
	last_update_date TIMESTAMP WITH TIME ZONE NOT NULL,
	active BOOLEAN NOT NULL
);

CREATE TABLE currencies 
(
	iso_code CHAR(3) PRIMARY KEY,
	currency_name VARCHAR(300) NOT NULL
);

CREATE TABLE bank_accounts 
(
	account_id BIGSERIAL PRIMARY KEY,
	user_id BIGINT REFERENCES users(user_id) NOT NULL,
	currency CHAR(3) REFERENCES currencies(iso_code) NOT NULL,
	iban VARCHAR(300),
	balance NUMERIC(38, 9),
	create_date TIMESTAMP WITH TIME ZONE NOT NULL,
	last_update_date TIMESTAMP WITH TIME ZONE NOT NULL,
	active BOOLEAN NOT NULL
);

CREATE TYPE transaction_type_enum AS ENUM ('WITHDRAW', 'DEPOSIT');

CREATE TABLE transcations 
(
	transaction_id BIGSERIAL PRIMARY KEY,
	account_id BIGINT REFERENCES bank_accounts(account_id) NOT NULL,
	amount NUMERIC(38, 9),
	transfer_date TIMESTAMP WITH TIME ZONE NOT NULL,
	transaction_type transaction_type_enum
);