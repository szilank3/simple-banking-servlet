package com.gitlab.szil.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.gitlab.szil.ds.DataSourceFactory;
import com.gitlab.szil.dto.BankAccountDTO;
import com.gitlab.szil.dto.TransactionDTO;
import com.gitlab.szil.dto.TransferDTO;
import com.gitlab.szil.enums.TransactionType;
import com.gitlab.szil.exception.ServiceDatabaseException;

/**
 * Service for handling transaction data and operations
 * 
 * @author Szilank
 *
 */
public class TransactionsService {

	private static TransactionsService instance;

	public static TransactionsService getInstance() {
		if (instance == null) {
			instance = new TransactionsService();
		}

		return instance;
	}

	private AccountService accountService = AccountService.getInstance();

	public List<TransactionDTO> findTransactionsByAccountId(long accountId) {
		List<TransactionDTO> result = new ArrayList<>();
		DataSource ds = DataSourceFactory.getDataSource();

		try (Connection conn = ds.getConnection();
				PreparedStatement stmt = conn //
						.prepareStatement("SELECT transaction_id, account_id, amount, transfer_date, transaction_type"
								+ "	FROM transcations WHERE account_id = ?")) {

			stmt.setLong(1, accountId);
			stmt.execute();

			try (ResultSet rs = stmt.getResultSet()) {
				while (rs.next()) {
					long transactionId = rs.getLong("transaction_id");
					long resAccountId = rs.getLong("account_id");
					BigDecimal amount = rs.getBigDecimal("amount");
					Date transferDate = rs.getDate("transfer_date");
					String transactionType = rs.getString("transaction_type");

					TransactionDTO transactionDTO = new TransactionDTO();

					BankAccountDTO bankAccount = accountService.findBankAccountById(resAccountId);

					transactionDTO.setTransactionId(transactionId);
					transactionDTO.setBankAccount(bankAccount);
					transactionDTO.setAmount(amount);
					transactionDTO.setTransferDate(transferDate);
					transactionDTO.setTransactionType(TransactionType.valueOf(transactionType));

					result.add(transactionDTO);
				}
			}
		} catch (SQLException e) {
			throw new ServiceDatabaseException("Error occured while executing statement. See cause:", e);
		}

		return result;
	}

	/**
	 * Saves transactions by TransferDTO. Withdraw from the source account and
	 * deposit to the destination
	 * 
	 * @param conn        "managed" connection
	 * 
	 * @param transferDTO not null
	 */
	public void saveNewTransactionByTransferDTO(Connection conn, TransferDTO transferDTO) throws SQLException {
		Date transferDate = new Date(Instant.now().toEpochMilli());

		try (PreparedStatement stmt = conn //
				.prepareStatement("INSERT INTO bank_schema.transcations(" //
						+ "	account_id, amount, transfer_date, transaction_type)" //
						+ "	VALUES (?, ?, ?, ?::transaction_type_enum)")) {

			stmt.setLong(1, transferDTO.getSourceBankAccount().getAccountId());
			stmt.setBigDecimal(2, transferDTO.getAmount());
			stmt.setDate(3, transferDate);
			stmt.setString(4, TransactionType.WITHDRAW.name());
			stmt.execute();
		}

		try (PreparedStatement stmt = conn //
				.prepareStatement("INSERT INTO bank_schema.transcations(" //
						+ "	account_id, amount, transfer_date, transaction_type)" //
						+ "	VALUES (?, ?, ?, ?::transaction_type_enum)")) {

			stmt.setLong(1, transferDTO.getDestinationBankAccount().getAccountId());
			stmt.setBigDecimal(2, transferDTO.getAmount());
			stmt.setDate(3, transferDate);
			stmt.setString(4, TransactionType.DEPOSIT.name());
			stmt.execute();
		}
	}

	private TransactionsService() {
		// Hide default constructor
	}

}
