package com.gitlab.szil.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import com.gitlab.szil.ds.DataSourceFactory;
import com.gitlab.szil.dto.UserDTO;
import com.gitlab.szil.exception.ServiceDatabaseException;

public class UserService {

	private static UserService instance = null;

	public static UserService getInstance() {
		if (instance == null) {
			instance = new UserService();
		}

		return instance;
	}

	public UserDTO findUserByUsername(String email) {
		DataSource ds = DataSourceFactory.getDataSource();

		try (Connection conn = ds.getConnection();
				PreparedStatement stmt = conn //
						.prepareStatement(
								"SELECT user_id, username, salt, password, email, create_date, last_update_date, active FROM users WHERE email = ?")) {

			stmt.setString(1, email);
			stmt.execute();

			try (ResultSet rs = stmt.getResultSet()) {
				while (rs.next()) {
					long userId = rs.getLong("user_id");
					String username = rs.getString("username");
					String salt = rs.getString("salt");
					String password = rs.getString("password");
					String rsEmail = rs.getString("email");
					Date createDate = rs.getDate("create_date");
					Date lastModifyDate = rs.getDate("last_update_date");
					boolean active = rs.getBoolean("active");

					UserDTO userDTO = new UserDTO();

					userDTO.setUserId(userId);
					userDTO.setUsername(username);
					userDTO.setSalt(salt);
					userDTO.setPassword(password);
					userDTO.setEmail(rsEmail);
					userDTO.setCreateDate(createDate);
					userDTO.setLastModifyDate(lastModifyDate);
					userDTO.setActive(active);

					return userDTO;
				}
			}
		} catch (SQLException e) {
			throw new ServiceDatabaseException("Error occured while executing statement. See cause:", e);
		}

		return null;
	}

	private UserService() {
		// Hide default constructor
	}

}
