package com.gitlab.szil.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import com.gitlab.szil.ds.DataSourceFactory;
import com.gitlab.szil.dto.BankAccountDTO;
import com.gitlab.szil.dto.TransferDTO;
import com.gitlab.szil.dto.UserDTO;
import com.gitlab.szil.exception.InsufficentFundsException;
import com.gitlab.szil.exception.ServiceDatabaseException;

public class AccountService {

	private static AccountService instance;

	public static AccountService getInstance() {
		if (instance == null) {
			instance = new AccountService();
		}

		return instance;
	}

	public List<BankAccountDTO> findBankAccountsByUser(UserDTO user) {
		List<BankAccountDTO> result = new ArrayList<>();
		DataSource ds = DataSourceFactory.getDataSource();

		try (Connection conn = ds.getConnection();
				PreparedStatement stmt = conn //
						.prepareStatement(
								"SELECT account_id, user_id, currency, iban, balance, create_date, last_update_date, active"
										+ "	FROM bank_accounts WHERE user_id = ?")) {

			stmt.setLong(1, user.getUserId());
			stmt.execute();

			try (ResultSet rs = stmt.getResultSet()) {
				while (rs.next()) {
					result.add(mapDTOFromResult(rs));
				}
			}
		} catch (SQLException e) {
			throw new ServiceDatabaseException("Error occured while executing statement. See cause:", e);
		}

		return result;
	}

	public BankAccountDTO findBankAccountById(long accountId) {
		BankAccountDTO result = null;
		DataSource ds = DataSourceFactory.getDataSource();

		try (Connection conn = ds.getConnection();
				PreparedStatement stmt = conn //
						.prepareStatement(
								"SELECT account_id, user_id, currency, iban, balance, create_date, last_update_date, active"
										+ "	FROM bank_accounts WHERE account_id = ?")) {

			stmt.setLong(1, accountId);
			stmt.execute();

			try (ResultSet rs = stmt.getResultSet()) {
				while (rs.next()) {
					result = mapDTOFromResult(rs);
				}
			}
		} catch (SQLException e) {
			throw new ServiceDatabaseException("Error occured while executing statement. See cause:", e);
		}

		return result;
	}

	/**
	 * Updates the source and destination accounts accordingly
	 * 
	 * @param conn2       "managed" connection
	 * @param transferDTO not null
	 * @throws InsufficentFundsException if the source account has not enough funds
	 */
	public void updateBankAccountsAfterTransfer(Connection conn, TransferDTO transferDTO)
			throws InsufficentFundsException, SQLException {
		try (PreparedStatement stmt = conn //
				.prepareStatement("SELECT balance FROM bank_accounts WHERE account_id = ?")) {

			stmt.setLong(1, transferDTO.getSourceBankAccount().getAccountId());
			stmt.execute();

			BigDecimal actualBalance = null;

			try (ResultSet rs = stmt.getResultSet()) {
				while (rs.next()) {
					actualBalance = rs.getBigDecimal(1);
				}
			}

			if (actualBalance != null && actualBalance.compareTo(transferDTO.getAmount()) < 0) {
				throw new InsufficentFundsException();
			}
		}

		try (PreparedStatement stmt = conn //
				.prepareStatement("UPDATE bank_accounts SET balance = (balance - ?), last_update_date = now() " //
						+ "WHERE account_id = ?")) {

			stmt.setBigDecimal(1, transferDTO.getAmount());
			stmt.setLong(2, transferDTO.getSourceBankAccount().getAccountId());
			stmt.executeUpdate();
		}

		try (PreparedStatement stmt = conn //
				.prepareStatement("UPDATE bank_accounts SET balance = (balance + ?), last_update_date = now() " //
						+ "WHERE account_id = ?")) {

			stmt.setBigDecimal(1, transferDTO.getAmount());
			stmt.setLong(2, transferDTO.getDestinationBankAccount().getAccountId());
			stmt.executeUpdate();
		}
	}

	private BankAccountDTO mapDTOFromResult(ResultSet rs) throws SQLException {
		long resAccountId = rs.getLong("account_id");
		long userId = rs.getLong("user_id");
		String currency = rs.getString("currency");
		String iban = rs.getString("iban");
		BigDecimal balance = rs.getBigDecimal("balance");
		Date createDate = rs.getDate("create_date");
		Date lastModifyDate = rs.getDate("last_update_date");
		boolean active = rs.getBoolean("active");

		BankAccountDTO accountDTO = new BankAccountDTO();

		accountDTO.setAccountId(resAccountId);
		accountDTO.setUserId(userId);
		accountDTO.setIban(iban);
		accountDTO.setBalance(balance);
		accountDTO.setCurrency(currency);
		accountDTO.setCreateDate(createDate);
		accountDTO.setLastModifyDate(lastModifyDate);
		accountDTO.setActive(active);

		return accountDTO;
	}

	private AccountService() {
		// Hide default constructor
	}

}
