package com.gitlab.szil.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.gitlab.szil.ds.DataSourceFactory;
import com.gitlab.szil.dto.BankAccountDTO;
import com.gitlab.szil.dto.TransferDTO;
import com.gitlab.szil.enums.TransferOption;
import com.gitlab.szil.exception.InsufficentFundsException;
import com.gitlab.szil.exception.ServiceDatabaseException;
import com.gitlab.szil.util.AppConstants;
import com.gitlab.szil.util.Pages;

public class TransferService {

	private static TransferService instance;

	public static TransferService getInstance() {
		if (instance == null) {
			instance = new TransferService();
		}

		return instance;
	}

	private TransactionsService transactionsService = TransactionsService.getInstance();

	private AccountService accountService = AccountService.getInstance();

	public void handleTransferRequestWithOptions(TransferOption option, HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		switch (option) {
		case CHECK:
			doCheck(req, resp);
			break;
		case SUBMIT:
			doSubmit(req, resp);
			break;
		case CANCEL:
			doCancel(req, resp);
			break;

		default:
			throw new IllegalArgumentException("Option not implemented yet.");
		}
	}

	private void doCheck(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String srcAccIdParam = req.getParameter(AppConstants.TRANSFER_SRC_ACC_PARAM_PROP);
		String dstAccIdParam = req.getParameter(AppConstants.TRANSFER_DST_ACC_PARAM_PROP);
		String amountParam = req.getParameter(AppConstants.TRANSFER_AMOUNT_PARAM_PROP);

		if (srcAccIdParam == null || dstAccIdParam == null || amountParam == null) {
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP, "Every field should be filled.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		Long srcAccId = null;
		Long destAccId = null;
		BigDecimal amount = null;

		try {
			srcAccId = Long.parseLong(srcAccIdParam);
		} catch (NumberFormatException e) {
			// Only should happens if someone manipulates the page
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP, "Source account ID is invalid.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		try {
			destAccId = Long.parseLong(dstAccIdParam);
		} catch (NumberFormatException e) {
			// Only should happens if someone manipulates the page
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP, "Destination account ID is invalid.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		try {
			amount = BigDecimal.valueOf(Double.parseDouble(amountParam));
		} catch (NumberFormatException e) {
			// Only should happens if someone manipulates the page
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP, "Could not parse amount parameter.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		if (amount == null || amount.signum() < 0 || amount.stripTrailingZeros().scale() > 0) {
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP, "The amount should be a whole positive number.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		if (Long.compare(srcAccId, destAccId) == 0) {
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP,
					"The source and the destination account should be not the same.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		BankAccountDTO sourceAcc = accountService.findBankAccountById(srcAccId);
		BankAccountDTO destAcc = accountService.findBankAccountById(destAccId);

		BigDecimal sourceAccBalance = sourceAcc.getBalance();

		if (sourceAccBalance.compareTo(amount) < 0) {
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP,
					"Source account does not have enough funds to complete this transfer.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		TransferDTO transfer = new TransferDTO();

		transfer.setSourceBankAccount(sourceAcc);
		transfer.setDestinationBankAccount(destAcc);
		transfer.setAmount(amount);

		HttpSession session = req.getSession(false);
		session.setAttribute(AppConstants.TRANSFER_STATE_CTX_PROP, transfer);

		req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
	}

	private void doSubmit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);
		TransferDTO transferDTO = (TransferDTO) session.getAttribute(AppConstants.TRANSFER_STATE_CTX_PROP);

		if (transferDTO == null) {
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP, "Transfer state should be exsits.");
			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
			return;
		}

		DataSource ds = DataSourceFactory.getDataSource();

		try (Connection conn = ds.getConnection()) {
			conn.setAutoCommit(false);

			transactionsService.saveNewTransactionByTransferDTO(conn, transferDTO);

			try {
				accountService.updateBankAccountsAfterTransfer(conn, transferDTO);
			} catch (InsufficentFundsException e) {
				req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP,
						"The source account doesn't has enough funds to complete this transfer.");
				req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
				return;
			}

			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new ServiceDatabaseException("Error occured while executing statement. See cause:", e);
		}

		req.getRequestDispatcher(Pages.DASHBOARD).forward(req, resp);
	}

	private void doCancel(HttpServletRequest req, HttpServletResponse resp) {
		HttpSession session = req.getSession(false);

		session.removeAttribute(AppConstants.TRANSFER_STATE_CTX_PROP);
	}

	private TransferService() {
		// Hide defeault constructor
	}

}
