package com.gitlab.szil.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.gitlab.szil.enums.TransactionType;

public class TransactionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long transactionId;
	private BankAccountDTO bankAccount;
	private BigDecimal amount;
	private Date transferDate;
	private TransactionType transactionType;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public BankAccountDTO getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccountDTO bankAccount) {
		this.bankAccount = bankAccount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

}
