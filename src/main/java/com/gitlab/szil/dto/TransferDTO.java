package com.gitlab.szil.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TransferDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private BankAccountDTO sourceBankAccount;
	private BankAccountDTO destinationBankAccount;
	private BigDecimal amount;
	private Date transferDate;

	public BankAccountDTO getSourceBankAccount() {
		return sourceBankAccount;
	}

	public void setSourceBankAccount(BankAccountDTO sourceBankAccount) {
		this.sourceBankAccount = sourceBankAccount;
	}

	public BankAccountDTO getDestinationBankAccount() {
		return destinationBankAccount;
	}

	public void setDestinationBankAccount(BankAccountDTO destinationBankAccount) {
		this.destinationBankAccount = destinationBankAccount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

}
