package com.gitlab.szil;

public class BankingAppException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BankingAppException(String message, Throwable cause) {
		super(message, cause);
	}

	public BankingAppException(String message) {
		super(message);
	}

	public BankingAppException(Throwable cause) {
		super(cause);
	}

}
