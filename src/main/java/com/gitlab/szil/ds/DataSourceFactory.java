package com.gitlab.szil.ds;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * A Singleton-Factory class for managing the DataSource instance.
 * 
 * @author Szilank
 *
 */
public class DataSourceFactory {

	private static DataSource instance = null;

	public static DataSource getDataSource() {
		if (instance == null) {
			instance = createNewDataSource();
		}

		return instance;
	}

	private static DataSource createNewDataSource() {
		HikariConfig config = new HikariConfig("/datasource/local.properties");
		return new HikariDataSource(config);
	}

	private DataSourceFactory() {
		// Hide the public constructor
	}

}
