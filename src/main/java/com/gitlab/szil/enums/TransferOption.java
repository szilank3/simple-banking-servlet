package com.gitlab.szil.enums;

public enum TransferOption {

	CHECK("check"), //
	SUBMIT("submit"), //
	CANCEL("cancel");

	private final String paramValue;

	private TransferOption(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamValue() {
		return paramValue;
	}

	public static TransferOption getByParamValue(String paramValue) {
		TransferOption[] options = values();
		for (int i = 0; i < options.length; i++) {
			TransferOption opt = options[i];
			if (opt.paramValue.equalsIgnoreCase(paramValue)) {
				return opt;
			}
		}

		return null;
	}

}
