package com.gitlab.szil.enums;

public enum TransactionType {

	WITHDRAW("Withdraw"), //
	DEPOSIT("Deposit");

	private final String displayName;

	private TransactionType(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

}
