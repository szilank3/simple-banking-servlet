package com.gitlab.szil.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gitlab.szil.dto.BankAccountDTO;
import com.gitlab.szil.dto.UserDTO;
import com.gitlab.szil.enums.TransferOption;
import com.gitlab.szil.service.AccountService;
import com.gitlab.szil.service.TransferService;
import com.gitlab.szil.util.AppConstants;
import com.gitlab.szil.util.Pages;

/**
 * Servlet for handling the transfer requests.
 * 
 * @author Szilank
 *
 */
@SuppressWarnings("serial")
@WebServlet("/secured/transfer")
public class TransferServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);

		if (session != null) {
			UserDTO user = (UserDTO) session.getAttribute(AppConstants.USER_CTX_PROP);

			if (user == null) {
				resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Must login first before accessing this page.");
				return;
			}

			AccountService accountService = AccountService.getInstance();
			List<BankAccountDTO> accountsForUser = accountService.findBankAccountsByUser(user);

			req.setAttribute(AppConstants.BANK_ACCOUNTS_CTX_PROP, accountsForUser);

			req.getRequestDispatcher(Pages.TRANSFER).forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);

		if (session != null) {
			UserDTO user = (UserDTO) session.getAttribute(AppConstants.USER_CTX_PROP);

			if (user == null) {
				resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Must login first before accessing this page.");
				return;
			}

			String optionParam = req.getParameter(AppConstants.TRANSFER_OPTION_PARAM_PROP);

			if (optionParam == null || optionParam.isEmpty()) {
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "'option' argument is missing.");
				return;
			}

			TransferOption transferOption = TransferOption.getByParamValue(optionParam);

			if (transferOption == null) {
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "The given 'option' argument is invalid.");
				return;
			}

			TransferService transferService = TransferService.getInstance();
			transferService.handleTransferRequestWithOptions(transferOption, req, resp);
		}
	}

}
