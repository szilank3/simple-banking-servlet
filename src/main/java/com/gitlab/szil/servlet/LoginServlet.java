package com.gitlab.szil.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gitlab.szil.crypto.CryptographyService;
import com.gitlab.szil.dto.UserDTO;
import com.gitlab.szil.service.UserService;
import com.gitlab.szil.util.AppConstants;
import com.gitlab.szil.util.Pages;

@WebServlet("/login")
@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.sendRedirect(req.getContextPath());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		String pass = req.getParameter("pass");

		UserService userService = UserService.getInstance();

		UserDTO userDto = userService.findUserByUsername(email);
		boolean isLoginCorrect = false;

		if (userDto != null) {
			CryptographyService cryptoService = CryptographyService.getInstance();
			isLoginCorrect = cryptoService.validatePassword(userDto.getSalt(), pass.toCharArray(),
					userDto.getPassword());
		}

		if (isLoginCorrect) {
			HttpSession session = req.getSession(false);

			if (session != null) {
				session.setAttribute(AppConstants.USER_CTX_PROP, userDto);
				session.setAttribute(AppConstants.USERNAME_CTX_PROP, userDto.getUsername());
				session.setAttribute(AppConstants.EMAIL_CTX_PROP, userDto.getEmail());
			}

			resp.sendRedirect(req.getContextPath() + "/secured/dashboard");
		} else {
			req.setAttribute(AppConstants.ERROR_MSG_CTX_PROP, "Email address or password is not correct.");
			req.getRequestDispatcher(Pages.INDEX).forward(req, resp);
		}
	}

}
