package com.gitlab.szil.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.szil.dto.TransactionDTO;
import com.gitlab.szil.dto.UserDTO;
import com.gitlab.szil.service.TransactionsService;
import com.gitlab.szil.util.AppConstants;
import com.gitlab.szil.util.Pages;

@WebServlet("/secured/transactions")
@SuppressWarnings("serial")
public class TransactionsServlet extends HttpServlet {

	private static final Logger logger = LoggerFactory.getLogger(TransactionsServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);

		if (session != null) {
			UserDTO user = (UserDTO) session.getAttribute(AppConstants.USER_CTX_PROP);

			if (user == null) {
				resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Must login first before accessing this page.");
				return;
			}

			String accountIdParam = req.getParameter(AppConstants.ACCOUNT_ID_PARAM_PROP);

			if (accountIdParam != null) {
				long accountId;
				try {
					accountId = Long.parseLong(accountIdParam);

				} catch (NumberFormatException e) {
					logger.warn("Invalid account_id [{}].", accountIdParam);
					resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not parse the given account_id.");
					return;
				}

				TransactionsService transactionsService = TransactionsService.getInstance();

				List<TransactionDTO> transactions = transactionsService.findTransactionsByAccountId(accountId);
				req.setAttribute(AppConstants.TRANSACTIONS_CTX_PROP, transactions);

				req.getRequestDispatcher(Pages.TRANSACTIONS).forward(req, resp);
			} else {
				resp.sendRedirect(req.getContextPath() + "/secured/dashboard");
			}
		}
	}

}
