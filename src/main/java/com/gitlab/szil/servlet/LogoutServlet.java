package com.gitlab.szil.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gitlab.szil.util.AppConstants;

/**
 * Servlet for handling logout. Removes user attribute (just in case) and
 * invalidates the session associated with the user.
 * 
 * @author Szilank
 *
 */
@WebServlet("/logout")
@SuppressWarnings("serial")
public class LogoutServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(false);

		if (session != null) {
			session.removeAttribute(AppConstants.USER_CTX_PROP);
			session.invalidate();
		}

		resp.sendRedirect(req.getContextPath() + "/");
	}

}
