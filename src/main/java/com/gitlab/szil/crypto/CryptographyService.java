package com.gitlab.szil.crypto;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.gitlab.szil.util.HexUtil;

/**
 * Service for generating secure password hash and validating
 * 
 * @author Szilank
 *
 */
public final class CryptographyService {

	private static final int KEY_LENGHT = 64 * 8;
	private static final String ALGORITHM = "PBKDF2WithHmacSHA1";
	private static final int ITERATIONS = 33333;
	private static final int SALT_LENGHT = 16;

	private static CryptographyService instance = null;

	public static CryptographyService getInstance() {
		if (instance == null) {
			instance = new CryptographyService();
		}

		return instance;
	}

	public PasswordHash hashPassword(char[] pass) {
		try {
			byte[] salt = generateSalt();

			PBEKeySpec spec = new PBEKeySpec(pass, salt, ITERATIONS, KEY_LENGHT);
			SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
			byte[] hash = skf.generateSecret(spec).getEncoded();

			return new PasswordHash(ITERATIONS, HexUtil.toHexString(salt), HexUtil.toHexString(hash));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new CryptographyException("Could not hash password. See cause:", e);
		}
	}

	public boolean validatePassword(String salt, char[] inputPass, String storedPass) {
		try {
			PBEKeySpec spec = new PBEKeySpec(inputPass, HexUtil.hexStringToByte(salt), ITERATIONS, KEY_LENGHT);
			SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
			byte[] testHash = skf.generateSecret(spec).getEncoded();
			byte[] storedHash = HexUtil.hexStringToByte(storedPass);

			int diff = storedHash.length ^ testHash.length;
			for (int i = 0; i < storedHash.length && i < testHash.length; i++) {
				diff |= storedHash[i] ^ testHash[i];
			}
			return diff == 0;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new CryptographyException("Could not validate password. See cause:", e);
		}

	}

	private byte[] generateSalt() throws NoSuchAlgorithmException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[SALT_LENGHT];
		sr.nextBytes(salt);
		return salt;
	}

	private CryptographyService() {
		// Hide default constructor
	}

}
