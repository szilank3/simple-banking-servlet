package com.gitlab.szil.crypto;

import com.gitlab.szil.BankingAppException;

public class CryptographyException extends BankingAppException {

	private static final long serialVersionUID = 1L;

	public CryptographyException(String message, Throwable cause) {
		super(message, cause);
	}

}
