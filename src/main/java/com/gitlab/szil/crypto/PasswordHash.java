package com.gitlab.szil.crypto;

public class PasswordHash {

	private final int iterations;
	private final String salt;
	private final String hash;

	public PasswordHash(int iterations, String salt, String hash) {
		this.iterations = iterations;
		this.salt = salt;
		this.hash = hash;
	}

	public int getIterations() {
		return iterations;
	}

	public String getSalt() {
		return salt;
	}

	public String getHash() {
		return hash;
	}

	@Override
	public String toString() {
		return String.join(":", Integer.toString(iterations), salt, hash);
	}

}
