package com.gitlab.szil;

import java.util.Arrays;

import com.gitlab.szil.util.HexUtil;

public class TestHex {

	public static void main(String[] args) {
		byte[] testByte = new byte[] { -2, -1, 0, 1, 2, 3, 4, 5 };
		System.out.println("Test byte" + Arrays.toString(testByte));

		String hexString = HexUtil.toHexString(testByte);

		System.out.println(hexString);

		byte[] stringToByte = HexUtil.hexStringToByte(hexString);
		System.out.println("Test byte after" + Arrays.toString(stringToByte));
	}

}
