package com.gitlab.szil.util;

public class Pages {

	public static final String INDEX = "index.jsp";
	public static final String DASHBOARD = "/WEB-INF/pages/dashboard.jsp";
	public static final String TRANSACTIONS = "/WEB-INF/pages/transactions.jsp";
	public static final String TRANSFER = "/WEB-INF/pages/transfer.jsp";

	private Pages() {
		// Hide default constructor
	}

}
