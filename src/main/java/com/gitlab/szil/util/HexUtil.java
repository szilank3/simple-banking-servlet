package com.gitlab.szil.util;

public class HexUtil {

	public static String toHexString(byte[] input) {
		StringBuffer buffer = new StringBuffer();

		for (int i = 0; i < input.length; i++) {
			buffer.append(Character.forDigit((input[i] >> 4) & 0xF, 16));
			buffer.append(Character.forDigit((input[i] & 0xF), 16));
		}

		int paddingLength = (input.length * 2) - buffer.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + buffer.toString();
		} else {
			return buffer.toString();
		}
	}

	public static byte[] hexStringToByte(String input) {
		int lenght = input.length();
		byte[] result = new byte[lenght / 2];

		for (int i = 0; i < lenght; i += 2) {
			result[i / 2] = (byte) ((Character.digit(input.charAt(i), 16) << 4)
					+ Character.digit(input.charAt(i + 1), 16));
		}

		return result;
	}

	private HexUtil() {
		// Utility class
	}

}
