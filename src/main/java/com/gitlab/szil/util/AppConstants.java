package com.gitlab.szil.util;

public final class AppConstants {

	public static final String USER_CTX_PROP = "user";
	public static final String USERNAME_CTX_PROP = "username";
	public static final String EMAIL_CTX_PROP = "email";
	public static final String ERROR_MSG_CTX_PROP = "errorMessage";
	public static final String BANK_ACCOUNTS_CTX_PROP = "accounts";
	public static final String TRANSACTIONS_CTX_PROP = "transactions";
	public static final String TRANSFER_STATE_CTX_PROP = "transfer_state";

	public static final String ACCOUNT_ID_PARAM_PROP = "account_id";
	public static final String TRANSFER_OPTION_PARAM_PROP = "option";
	public static final String TRANSFER_SRC_ACC_PARAM_PROP = "source_account";
	public static final String TRANSFER_DST_ACC_PARAM_PROP = "destination_account";
	public static final String TRANSFER_AMOUNT_PARAM_PROP = "amount";

	private AppConstants() {
		// Hide default constructor
	}

}
