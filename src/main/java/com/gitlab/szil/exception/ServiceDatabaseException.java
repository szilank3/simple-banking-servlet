package com.gitlab.szil.exception;

public final class ServiceDatabaseException extends AbstractApplicationException {

	private static final long serialVersionUID = 1L;

	public ServiceDatabaseException(String message, Throwable cause) {
		super(message, cause);
	}

}
