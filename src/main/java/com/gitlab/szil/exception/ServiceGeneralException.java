package com.gitlab.szil.exception;

public final class ServiceGeneralException extends AbstractApplicationException {

	private static final long serialVersionUID = 1L;

	public ServiceGeneralException(String message, Throwable cause) {
		super(message, cause);
	}

}
