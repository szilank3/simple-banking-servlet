package com.gitlab.szil.exception;

@SuppressWarnings("serial")
public abstract class AbstractApplicationException extends RuntimeException {

	public AbstractApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

}
