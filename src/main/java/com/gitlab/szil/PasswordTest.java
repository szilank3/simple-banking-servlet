package com.gitlab.szil;

import com.gitlab.szil.crypto.CryptographyService;
import com.gitlab.szil.crypto.PasswordHash;

public class PasswordTest {

	public static void main(String[] args) {
		CryptographyService cryptoService = CryptographyService.getInstance();

		String pass1 = "Jelszo123";
		String pass2 = "Jelszo456";

		PasswordHash pass1Hash = cryptoService.hashPassword(pass1.toCharArray());
		PasswordHash pass2Hash = cryptoService.hashPassword(pass2.toCharArray());

		System.out.println("Password1: " + pass1Hash.toString());
		System.out.println("Password2: " + pass2Hash.toString());
	}

}
