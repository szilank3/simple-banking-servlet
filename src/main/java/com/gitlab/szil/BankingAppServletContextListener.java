package com.gitlab.szil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.szil.ds.DataSourceFactory;

@WebListener
public class BankingAppServletContextListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(BankingAppServletContextListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		logger.info("Servlet init.");
		Flyway flyway = Flyway.configure().dataSource(DataSourceFactory.getDataSource()).load();
		flyway.migrate();
	}
}
