package com.gitlab.szil.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.gitlab.szil.util.AppConstants;

@ExtendWith(MockitoExtension.class)
@DisplayName("Transfer servlet tests")
public class TransferServletTest {

	@Mock
	HttpServletRequest req;
	@Mock
	HttpServletResponse resp;
	@Mock
	HttpSession session;

	@BeforeAll
	static void initMocks() {
		MockitoAnnotations.initMocks(TransferServletTest.class);
	}

	@Test
	void unauthorizedRequestTest() throws Exception {
		TransferServlet servlet = Mockito.mock(TransferServlet.class);
		Mockito.when(req.getSession(false)).thenReturn(session);
		Mockito.when(session.getAttribute(AppConstants.USER_CTX_PROP)).thenReturn(null);

		Mockito.doCallRealMethod().when(servlet).doGet(req, resp);
		servlet.doGet(req, resp);

		Mockito.verify(resp).sendError(HttpServletResponse.SC_UNAUTHORIZED,
				"Must login first before accessing this page.");
	}

}
